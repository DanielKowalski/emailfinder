﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmailFinder.Models;
using EmailFinder.Services;

namespace EmailFinder.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewModel model = new ViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(ViewModel model)
        {
            if (ModelState.IsValid)
            {
                LeadsGenerator generator = new LeadsGenerator(model.Data);
                var leads = generator.GenerateLeads();
                MailVeryfier veryfier = new MailVeryfier(leads);
                var veryfiredMails = veryfier.VerifyMails().ToList();
                model.Good = veryfiredMails[0];
                model.Bad = veryfiredMails[1];
            }
            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
