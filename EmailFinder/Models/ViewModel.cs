﻿using System;
using System.Collections.Generic;

namespace EmailFinder.Models
{
    public class ViewModel
    {
        public ViewModel() 
        {
            Good = new List<string>();
            Bad = new List<string>();
        }

        public MailData Data { get; set; }
        public IEnumerable<string> Good { get; set; }
        public IEnumerable<string> Bad { get; set; }
    }
}
