﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmailFinder.Models
{
    public class MailData
    {
        [Required(ErrorMessage = "Nie podano imienia!")]
        [Display(Name = "Imię:")]
        [RegularExpression("[a-zA-Zzażółćgęśląjażżń]+", ErrorMessage = "Imię nie powinno zawierać tylko litery!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nie podano nazwiska!")]
        [Display(Name = "Nazwisko:")]
        [RegularExpression("[a-zA-Zzażółćgęśląjażżń]+", ErrorMessage = "Nazwisko nie powinno zawierać tylko litery!")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Nie podano domeny!")]
        [Display(Name = "Domena:")]
        [RegularExpression(@"([^.]+\.[^.]+){1}(\.[^.])*", ErrorMessage = "Niewłaściwy format domeny!")]
        public string Domain { get; set; }
    }
}
