﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using DnsClient;

namespace EmailFinder.Services
{
    public class MailVeryfier
    {
        private readonly List<string> _mailsToCheck;
        private readonly string _server;

        public MailVeryfier(List<string> mailsToCheck)
        {
            _mailsToCheck = mailsToCheck;
            _server = _mailsToCheck.Count > 0 ? GetBestServer() : "";
        }

        public IEnumerable<List<string>> VerifyMails()
        {
            List<string> goodMails = new List<string>();
            List<string> badMails = new List<string>();

            if (!string.IsNullOrEmpty(_server))
            {
                var comparision = StringComparison.CurrentCulture;
                using (TcpClient client = new TcpClient(_server, 25))
                using (NetworkStream stream = client.GetStream())
                using (StreamWriter writer = new StreamWriter(stream)
                                                { AutoFlush = true })
                using (StreamReader reader = new StreamReader(stream))
                {
                    if (!reader.ReadLine().StartsWith("220", comparision))
                    {
                        throw new InvalidOperationException("Connect error!");
                    }
                    writer.WriteLine("HELO gmail.com");
                    if (!reader.ReadLine().StartsWith("250", comparision))
                    {
                        throw new InvalidOperationException("HELO error!");
                    }
                    writer.WriteLine("MAIL FROM: <testitdevelop@gmail.com>");
                    if (!reader.ReadLine().StartsWith("250", comparision))
                    {
                        throw new InvalidOperationException("MAIL FROM error!");
                    }
                    foreach (string mail in _mailsToCheck)
                    {
                        writer.WriteLine($"RCPT TO: <{mail}>");
                        if (reader.ReadLine().StartsWith("250", comparision))
                        {
                            goodMails.Add(mail);
                        }
                        else
                        {
                            badMails.Add(mail);
                        }
                    }
                    writer.WriteLine("QUIT");
                }
            }
            return new List<List<string>> { goodMails, badMails };
        }

        private string GetBestServer()
        {
            string domain = _mailsToCheck[0].Split("@")[1];
            LookupClient lookup = new LookupClient();
            var result = lookup.Query(domain, QueryType.MX);
            if (result.Answers.Count > 0)
            {
                int min = int.Parse(result.Answers[0].ToString().Split(" ")[3]);
                foreach (var answer in result.Answers)
                {
                    int temp = int.Parse(answer.ToString().Split(" ")[3]);
                    if (temp < min)
                    {
                        min = temp;
                    }
                }
                foreach (var answer in result.Answers)
                {
                    int temp = int.Parse(answer.ToString().Split(" ")[3]);

                    if (temp == min)
                    {
                        string serverWithDot = answer.ToString().Split(" ")[4];
                        string server = serverWithDot
                                        .Remove(serverWithDot.Length - 1);
                        return server;

                    }
                }
            }
            return "";
        }
    }
}
