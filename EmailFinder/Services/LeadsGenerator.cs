﻿using System;
using System.Collections.Generic;
using EmailFinder.Models;

namespace EmailFinder.Services
{
    public class LeadsGenerator
    {
        private readonly string _name;
        private readonly string _surname;
        private readonly string _domain;

        public LeadsGenerator(MailData data)
        {
            _name = RemovePolishChars(data.Name.ToLower());
            _surname = RemovePolishChars(data.Surname.ToLower());
            _domain = data.Domain;
        }

        private string RemovePolishChars(string text)
        {
            string result = text.Replace('ą', 'a')
                                .Replace('ż', 'z')
                                .Replace('ź', 'z')
                                .Replace('ć', 'c')
                                .Replace('ń', 'n')
                                .Replace('ś', 's')
                                .Replace('ł', 'l')
                                .Replace('ę', 'e')
                                .Replace('ó', 'o');
            return result;
        }

        public List<string> GenerateLeads() 
        {
            List<string> leads = new List<string>
            {
                GenerateLead(_name),
                GenerateLead(_surname)
            };
            leads.AddRange(GenerateLeadWithSeparator(""));
            leads.AddRange(GenerateLeadWithSeparator("."));
            leads.AddRange(GenerateLeadWithSeparator("-"));
            leads.AddRange(GenerateLeadWithSeparator("_"));

            return leads;
        }

        private List<string> GenerateLeadWithSeparator(string separator)
        {
            List<string> leadsWithSeparator = new List<string>
            {
                GenerateLead(_name + separator + _surname),
                GenerateLead(_surname + separator + _name),
                GenerateLead(_name[0] + separator + _surname),
                GenerateLead(_surname[0] + separator + _name),
                GenerateLead(_name[0] + separator + _surname[0]),
                GenerateLead(_surname[0] + separator + _name[0]),
                GenerateLead(_name + separator + _surname[0]),
                GenerateLead(_surname + separator + _name[0])
            };

            return leadsWithSeparator;
        }

        private String GenerateLead(string mail)
        {
            return $"{mail}@{_domain}";
        }
    }
}
