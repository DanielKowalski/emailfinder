# EmailFinder
Aplikacja generuje adresy email używając podanych imienia i nazwiska oraz domeny. Nastepnie weryfikuje, które z nich faktycznie instnieją na serwerze pocztowym. Wypisuje wygenerowane maila dzieląc je na istniejące i nieistniejące.

##Ostrzeżenie:
**Nie wszystkie serwery pocztowe współpracują z aplikacją!** Niektóre posiadają odpowiednie zabezpieczenie, np. wp.pl po paru próbach weryfikacji wpisało mój adres IP na czarną listę za zachowania spamerskie. Z tego co wyczytałem niektóre serwery są tak skonfigurowane, aby zawsze odpowiadać na tego typu zapytania OK niezależnie czy mail istnieje w systemie czy nie. Inne ustawiają czas odpowiedzi na komendy np. na 15 minut co wysypywało się na moim hostingu, akceptującym maksymalnie 30 sekundowe opóźnienie. 

###Serwery, które przetestowałem na wersji javowej na hostingu zewnętrznym:
- gmail.com (również z GSuite)
- interia.pl

##Autor
Daniel Kowalski

[Kontakt](mailto:danielk.biznes@gmail.com)
